# install homebrew

<https://brew.sh/>

> /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"


# then, install bundle

    export HOMEBREW_CASK_OPTS="--no-quarantine"; brew bundle 
    sh macos.sh

# remove all dock item, if want

    defaults write com.apple.dock persistent-apps -array